
setup_nexus(){
    echo "Create docker volume and run nexus container"
    docker volume create --name nexus-data;
    docker run -d -p 8081:8081 --name nexus -v nexus-data:/nexus-data sonatype/nexus3;
    docker logs -f nexus

    echo "Wait for nexus to start"
    command = "curl -u admin:kjmNVps4kTU6Fo -s -o /dev/null -w "%{http_code}" http://localhost:8081"
    while [ "$command" != "200" ]; do
        echo "Waiting for nexus to start"
        sleep 5
    done

    echo "Create repository if not exists"
    # Is the test repo already created?
    repo_name = "test"
    curl -X 'GET' \
        'http://localhost:8081/service/rest/v1/repositories/'$repo_name \
        -H 'accept: application/json' \
        -H 'NX-ANTI-CSRF-TOKEN: 0.2566926141088446' \
        -H 'X-Nexus-UI: true'

    if [ $? -eq 0 ]; then
        echo "Repository already exists"
    else
        echo "Create repository"
        curl -X 'POST' \
            'http://localhost:8081/service/rest/v1/repositories/raw/hosted' \
            -H 'accept: application/json' \
            -H 'Content-Type: application/json' \
            -H 'NX-ANTI-CSRF-TOKEN: 0.2566926141088446' \
            -H 'X-Nexus-UI: true' \
            -d '{
            "name": "'$repo_name'",
            "online": true,
            "storage": {
                "blobStoreName": "default",
                "strictContentTypeValidation": true,
                "writePolicy": "allow_once"
            },
            "cleanup": {
                "policyNames": [
                "string"
                ]
            },
            "component": {
                "proprietaryComponents": true
            },
            "raw": {
                "contentDisposition": "ATTACHMENT"
            }
            }'
    fi



}

setup_nexus()