#!/bin/bash
apt update -y
apt upgrade -y

# Update pip
echo "Update pip"
pip install --upgrade pip

# Git clone projects if not already cloned
git clone git@gitlab.com:leap_tech/workspace-scripts.git /workspace/leaptech/workspace-scripts || true
git clone git@gitlab.com:leap_tech/aiop-group/aiop.git /workspace/leaptech/aiop/aiop || true
git clone git@gitlab.com:leap_tech/aiop-group/clients.git /workspace/leaptech/aiop/Clients || true
git clone git@gitlab.com:leap_tech/aiop-group/plugins/template.git /workspace/leaptech/aiop/Plugin/template || true
git clone git@gitlab.com:leap_tech/aiop-group/wiki.git /workspace/leaptech/aiop/wiki || true
git clone git@gitlab.com:leap_tech/images/docker-image.git /workspace/leaptech/ci-cd/cicd-docker-image || true
git clone git@gitlab.com:leap_tech/images/pipeline-template.git /workspace/leaptech/ci-cd/ci-cd-pipeline-template || true
git clone git@gitlab.com:leap_tech/playbooks/skeleton.git /workspace/leaptech/playbooks/skeleton || true
git clone git@gitlab.com:leap_tech/playbooks/templates/first-playbook-example.git /workspace/leaptech/playbooks/templates/first-playbook-example || true
git clone git@gitlab.com:leap_tech/playbooks/templates/ingenico-reference.git /workspace/leaptech/playbooks/templates/ingenico-playbook || true
git clone git@gitlab.com:leap_tech/aiop-group/licensing.git /workspace/leaptech/aiop/licensing || true
git clone git@gitlab.com:leap_tech/aiop-group/plugins/ingenico-plugin.git /workspace/leaptech/aiop/Plugin/ingenico-plugin || true
git clone git@github.com:Gamma-Software/aiop-docs.git /workspace/leaptech/aiop/docs || true

# Move workspace in root folder to trigger vscode to ask to open it with the workspace
ln -s /workspace/leaptech/workspace-scripts/leaptech.code-workspace /workspace/leaptech

# Set git config
git config --global user.name "$USER LeapTech devcontainer"
git config --global user.email "valentin.rudloff.perso@gmail.com"

# Install aiop Core
pip install -e /workspace/leaptech/aiop/aiop

# Install aiop user config
rm ~/.aiop/aiop.yml
ln -s /workspace/leaptech/workspace-scripts/user-config/aiop.yml ~/.aiop/aiop.yml

# Docker login
docker login

# Install the secrets
#source /workspace/aiop/.devcontainer/export_secrets.sh

# Add it to zshrc
#echo "source /workspace/aiop/.devcontainer/export_secrets.sh" >> ~/.bashrc
#echo "source /workspace/aiop/.devcontainer/export_secrets.sh" >> ~/.zshrc

# Install pyenv
curl https://pyenv.run | bash
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.profile
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.profile
echo 'eval "$(pyenv init -)"' >> ~/.profile
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc
echo '[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc
echo 'eval "$(pyenv init -)"' >> ~/.zshrc
source ~/.bashrc
source ~/.profile

# Install python versions
pyenv install 3.6 --skip-existing
pyenv install 3.7 --skip-existing
pyenv install 3.8 --skip-existing
pyenv install 3.9 --skip-existing
pyenv install 3.10 --skip-existing
pyenv install 3.11 --skip-existing
pyenv install 3.12 --skip-existing