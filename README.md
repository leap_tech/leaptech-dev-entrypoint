# Workspace Scripts

## Getting started

Create and open the workspace within a devcontainer with this command :

``` bash
bash <(curl -s https://gitlab.com/leap_tech/workspace-scripts/-/raw/main/run_dev.sh)
```
