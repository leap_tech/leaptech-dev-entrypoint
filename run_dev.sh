#!/bin/bash
source <(curl -s https://gitlab.com/leap_tech/workspace-scripts/-/raw/main/common_functions.sh)

platform=$(get_platform)
display_info "Script executed on $platform"

# Install docker (if necessary)
if ! command_exists docker; then
	display_info "Docker is not installed. Installing Docker..."
	case "${platform}" in
  "Linux")
    # Docker installation for Linux
    execute_command "sudo apt-get update" "Failed to update linux packages"
    execute_command "sudo apt-get install -y docker.io" "Failed to install docker"
    ;;
  "Mac")
    # Docker installation for Mac
    # Check if Homebrew is installed
    if ! command_exists brew; then
      execute_command "curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh" "Failed to install brew"
    fi
    execute_command "brew cask install docker" "Failed to install docker"
    ;;
  *)
    echo "Unsupported platform for Docker installation: ${platform}"
    exit 1
    ;;
  esac
  if ! command_exists docker; then
    display_error "Failed to install docker"
    exit 1
  fi
fi

# Check if Docker is running
if ! docker_is_running; then
  display_error "Docker is not running. Please start it then rerun this script"
  exit 1
fi

# Check if Docker Compose is installed
if ! command_exists docker-compose; then
  display_info "Docker Compose is not installed. Installing Docker Compose..."
  case "${platform}" in
    "Linux")
      # Docker Compose installation for Linux
      execute_command "sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose" "Failed to install docker-compose"
      execute_command "sudo chmod +x /usr/local/bin/docker-compose" "Failed to change docker-compose user rights"
      ;;
    "Mac")
      # Docker Compose installation for Mac
      # You can use Homebrew or another method here
      execute_command "brew install docker-compose" "Failed to install docker-compose"
      ;;
    *)
      echo "Unsupported platform for Docker Compose installation: ${platform}"
      exit 1
      ;;
  esac
  if ! command_exists docker-compose; then
    display_error "Failed to install docker-compose"
    exit 1
  fi
fi

# Installing development workspace
if ! command_exists code; then
    display_info "vscode not installed, please install it with this link https://code.visualstudio.com/download#"
    open_webpage "https://code.visualstudio.com/download#"
    continue_msg

    display_info "Then install code in your path"
    open_webpage "https://code.visualstudio.com/docs/setup/mac#_launching-from-the-command-line"
    continue_msg

    display_info "Then install devcontainer cli"
    open_webpage "https://code.visualstudio.com/docs/devcontainers/devcontainer-cli#_installation"
    continue_msg

    execute_command "code -h" "VSCode not install correctly, please process to the installation then rerun the script"
fi

# Installing devcontainer cli
if ! command_exists devcontainer; then
    execute_command "sudo npm install -g @devcontainers/cli" "Failed to change docker-compose user rights"
    if command_exists devcontainer; then
      echo "devcontainer installed correctly"
    else
      display_error "devcontainer not installed correctly, please create an issue to: https://gitlab.com/leap_tech/workspace-scripts/-/issues"
      open_webpage "https://gitlab.com/leap_tech/workspace-scripts/-/issues"
      exit 1
    fi
fi

# 6. Building devcontainers
display_info "Starting devcontainer"
add_ssh_key
if [ $? -ne 0 ]; then
  display_error "Failed to create SSH key"
  exit 1
fi

workspace="$HOME/.leaptech/"
rm -rf $workspace
execute_command "git clone git@gitlab.com:leap_tech/workspace-scripts.git $workspace" "Failed to clone the repo, you may have forgotten to add your public key to Gitlab, go to: https://gitlab.com/-/profile/keys"
cd $workspace
ls $workspace
execute_command "devcontainer open" "Failed to start the devcontainer"

exit 0